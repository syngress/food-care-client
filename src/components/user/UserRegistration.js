import React, { Component } from 'react'
import axios from 'axios'
import FormErrors from '../FormErrors'
// eslint-disable-next-line
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import Swal from 'sweetalert2'
import '../../custom_css/user.scss';

class User extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      username: '',
      email: '',
      password: '',
      password_confirmation: '',
      formErrors: {name: '', username: '', email: '', password: '', password_confirmation: ''},
      nameValid: false,
      usernameValid: false,
      emailValid: false,
      passwordValid: false,
      passwordConfirmationValid: false,
      formValid: false
    }

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleUserInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]: value},
                  () => { this.validateField(name, value) });
  }

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let nameValid = this.state.nameValid;
    let usernameValid = this.state.usernameValid;
    let emailValid = this.state.emailValid;
    let passwordValid = this.state.passwordValid;
    let passwordConfirmationValid = this.state.passwordConfirmationValid;

    switch(fieldName) {
      case 'name':
        nameValid = value.length >= 3;
        fieldValidationErrors.name = nameValid ? '' : ' is invalid';
        break;
      case 'username':
        usernameValid = value.length >= 3;
        fieldValidationErrors.username = usernameValid ? '' : 'is invalid';
        break;
      case 'email':
        emailValid = value.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i);
        fieldValidationErrors.email = emailValid ? '' : ' is invalid';
        break;
      case 'password':
        passwordValid = value.length >= 6;
        fieldValidationErrors.password = passwordValid ? '': ' is too short';
        break;
      case 'password_confirmation':
        passwordConfirmationValid = value.length >= 6;
        fieldValidationErrors.password_confirmation = passwordConfirmationValid ? '': ' is too short';
        break;
      default:
        break;
    }

    this.setState({
      formErrors: fieldValidationErrors,
      nameValid: nameValid,
      usernameValid: usernameValid,
      emailValid: emailValid,
      passwordValid: passwordValid,
      passwordConfirmationValid: passwordConfirmationValid },
      this.validateForm);
    }

  validateForm() {
    this.setState({formValid: this.state.nameValid && this.state.usernameValid && this.state.emailValid && this.state.passwordValid && this.state.passwordConfirmationValid});
  }

  handleFormSubmit(e){
      e.preventDefault();

      if (this.state.password !== this.state.password_confirmation) {
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'passwords don\'t match',
          footer: '<a href>Try again</a>'
        })
        return;
      }

      axios.post('http://localhost:3001/users', {
        name: this.state.name,
        username: this.state.username,
        email: this.state.email,
        password: this.state.password,
        password_confirmation: this.state.password_confirmation
      }).then(res => {
        let timerInterval
        Swal.fire({
          title: 'Your registration has been completed.',
          html: 'Redirect to login page in <strong></strong> seconds.',
          type: "success",
          timer: 4000,
          onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
              Swal.getContent().querySelector('strong')
                .textContent = Swal.getTimerLeft()
            }, 100)
          },
          onClose: () => {
            clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
            this.props.history.replace('/login')
          }
        })
      })
      .catch(err => {
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: this.handleError(err),
          footer: '<a href>Try to refresh the page</a>'
        })
      })
  }

  handleError(err) {
    if (!err.hasOwnProperty('response')) {
      err.response = {
          data: {
            details: "An Unexpected Error Occurred"
          }
       }
    }
    if (Object.keys(err.response.data.details).length !== 0) {
      var response_error = JSON.stringify(err.response.data.details);
      if (response_error) {
        response_error = response_error.slice(1, response_error.length-1).replace(/"/g, "")
        response_error = response_error.split(',')
        response_error = response_error.join("\r\n");
        return response_error
      }
    } else {
      return err.response.data.message
    }
  }

  errorClass(error) {
    return(error.length === 0 ? '' : 'is-invalid');
  }

  render() {
    var divStyle = {
      width: '100%'
    };
  return (
    <div className="registration-form">
      <div className="col-md-8 order-md-1">
        <h4 className="mb-3">User Registration</h4>
        <hr className="mb-4"></hr>
        <div className="panel panel-default">
          <FormErrors formErrors={this.state.formErrors} />
        </div>
        <form className="needs-validation">
          <div className="row">
            <div className="col-md-6 mb-3">
              <label>Name</label>
              <input
                type="text"
                className={`form-control ${this.errorClass(this.state.formErrors.name)}`}
                id="name"
                name="name"
                placeholder="Your Name"
                onChange={this.handleUserInput}
                value={this.state.name}
                required={true} />
                <div className="invalid-feedback" style={divStyle}>
                  Your name is required.
                </div>
            </div>
            <div className="col-md-6 mb-3">
              <label>Username</label>
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">@</span>
                </div>
                <input
                  type="text"
                  className={`form-control ${this.errorClass(this.state.formErrors.username)}`}
                  id="username"
                  name="username"
                  placeholder="Username"
                  onChange={this.handleUserInput}
                  value={this.state.username}
                  required={true} / >
                <div className="invalid-feedback" style={divStyle}>
                  Your username is required.
                </div>
              </div>
            </div>
          </div>
          <div className="mb-3">
            <label>Email <span className="text-muted">(Provide valid email address)</span></label>
            <input
              type="email"
              className={`form-control ${this.errorClass(this.state.formErrors.email)}`}
              id="email"
              name="email"
              placeholder="your@email.com"
              value={this.state.email}
              onChange={this.handleUserInput}
              required={true} />
            <div className="invalid-feedback">
              Please enter a valid email address for proper registration process.
            </div>
          </div>
          <div className="mb-3">
            <label>Password</label>
            <input
              type="password"
              className={`form-control ${this.errorClass(this.state.formErrors.password)}`}
              id="password"
              name="password"
              placeholder="Password"
              onChange={this.handleUserInput}
              value={this.state.password}
              required={true} />
            <div className="invalid-feedback">
              Please enter valid password.
            </div>
          </div>
          <div className="mb-3">
            <label>Repeat Password</label>
            <input
              type="password"
              className={`form-control ${this.errorClass(this.state.formErrors.password_confirmation)}`}
              id="password_confirmation"
              name="password_confirmation"
              placeholder="Repeat Password"
              onChange={this.handleUserInput}
              value={this.state.password_confirmation}
              required={true} />
          </div>
          <hr className="mb-4"></hr>
          <div className="row">
            <hr className="mb-4"></hr>
            <div className="col-md-6 mb-3">
              <button className="btn btn-primary btn-lg btn-block" type="submit" disabled={!this.state.formValid} onClick={this.handleFormSubmit}>Register</button>
            </div>
            <div className="col-md-6 mb-3">
            <Link className="btn btn-lg btn-block" to={{
              pathname: '/'
              }}>Get Back</Link>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
  }
}

export default User
