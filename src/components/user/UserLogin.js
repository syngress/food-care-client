import React, { Component } from 'react';
import Swal from 'sweetalert2'
import AuthService from '../../components/AuthService'
import '../../custom_css/login.scss';
import mainlogo from '../../custom_img/foodcare_mainlogo.jpg'
// eslint-disable-next-line
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

function validate(email, password) {
  return {
    email: email.length === 0,
    password: password.length === 0
  };
}

class Login extends Component {
  constructor() {
    super();
    this.state = {
     email: '',
     password: '',
     touched: {
       email: false,
       password: false,
     }
    };

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.Auth = new AuthService();
  }

  handleEmailChange = evt => {
    this.setState({ email: evt.target.value });
  };

  handlePasswordChange = evt => {
    this.setState({ password: evt.target.value });
  };

  handleBlur = (field) => (evt) => {
    this.setState({
      touched: { ...this.state.touched, [field]: true },
    });
  }

  componentWillMount(){
    if(this.Auth.loggedIn())
        this.props.history.replace('/');
  }

  handleFormSubmit(e){
      e.preventDefault();

      this.Auth.login(this.state.email,this.state.password)
          .then(res =>{
             this.props.history.replace('/');
          })
          .catch(err =>{
            Swal.fire({
              type: 'error',
              title: 'Oops...',
              text: err,
              footer: '<a href>Try to refresh the page</a>'
            })
          })
  }

  canBeSubmitted() {
    const errors = validate(this.state.email, this.state.password);
    const isDisabled = Object.keys(errors).some(x => errors[x]);
    return !isDisabled;
  }

  render() {
    const errors = validate(this.state.email, this.state.password);
    const isDisabled = Object.keys(errors).some(x => errors[x]);
    const shouldMarkError = field => {
      const hasError = errors[field];
      const shouldShow = this.state.touched[field];

      return hasError ? shouldShow : false;
    };

    return (
        <div className="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
          <header className="masthead mb-auto">
            <div className="inner">
            <img src={mainlogo} alt="Logo" className="image-center " />

              <nav className="nav nav-masthead justify-content-center">
                <Link className="nav-link active" to={{
                  pathname: '/register',
                }}>Register</Link>
                <a className="nav-link" href="http://localhost:3000">About</a>
                <a className="nav-link" href="http://localhost:3000">Contact</a>
              </nav>
            </div>
          </header>

          <form className="form-signin">
            <img className="mb-4" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72"></img>
            <h1 className="h3 mb-3 font-weight-normal text-center">Please sign in</h1>
            <label htmlFor="inputEmail" className="sr-only">Email address</label>
            <input
                type="email"
                id="inputEmail"
                className={shouldMarkError("email") ? "form-control-red" : "form-control"}
                name="email"
                placeholder="Enter your email"
                required
                autoFocus=""
                onChange={this.handleEmailChange}
                onBlur={this.handleBlur("email")}
                value={this.state.email} />
            <label htmlFor="inputPassword" className="sr-only">Password</label>
            <input
                type="password"
                id="inputPassword"
                className={shouldMarkError("password") ? "form-control-red" : "form-control"}
                name="password"
                placeholder="Password"
                required
                onChange={this.handlePasswordChange}
                onBlur={this.handleBlur("password")}
                value={this.state.password} />
            <div className="checkbox mb-3">
                <input type="checkbox" name="baseball" value="remember-me" onChange={this.onChange}/> Remember me
            </div>
            <button className="btn btn-lg btn-primary btn-block" type="submit" disabled={isDisabled} onClick={this.handleFormSubmit}>Sign in</button>
            <p className="mt-5 mb-3 text-muted">© Foodcare System</p>
          </form>
        </div>
    );
  }
}

export default Login;
