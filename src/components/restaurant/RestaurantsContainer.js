import React, { Component } from 'react'
import axios from 'axios'
// eslint-disable-next-line
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Pagination from '../Pagination'
import '../../custom_css/pagination.scss';
import '../../custom_css/restaurant.scss';
import checkAction from '../../custom_img/check.png'
import createAction from '../../custom_img/create.png'

class RestaurantsContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      allRestaurants: [],
      currentRestaurants: [],
      editingIdeaId: null,
      notification: '',
      transitionIn: false,
      currentPage: null,
      totalPages: null,
      token: {
          'Content-Type': 'application/json',
          'Authorization': localStorage.getItem('id_token')
      }
    }
  }

  componentDidMount() {
    if (this.props.restaurants && this.props.restaurants.length > 0) {
      const { data: allRestaurants = [] } = this.props.restaurants
      this.setState({ allRestaurants });
    } else {
      axios.get(`http://localhost:3001/restaurants`, { headers: this.state.token })
      .then(response => {
        const { data: allRestaurants = [] } = response.data
        this.setState({ allRestaurants });
      })
      .catch(error => console.log(error))
    }
  }

  onPageChanged = data => {
    const { currentPage, pageLimit } = data;
    if (this.state.token) {
      axios.get(`http://localhost:3001/restaurants?page=${currentPage}&per_page=${pageLimit}`, { headers: this.state.token })
      .then(response => {
        const { allRestaurants } = this.state;
        const { currentPage, totalPages, pageLimit } = data;
        const offset = (currentPage - 1) * pageLimit;
        const currentRestaurants = allRestaurants.slice(offset, offset + pageLimit);
        this.setState({ currentPage, currentRestaurants, totalPages });
      })
      .catch(error => console.log(error))
    }
  }

  render() {
    const { allRestaurants, currentRestaurants, currentPage, totalPages } = this.state;
    const totalRestaurants = allRestaurants.length;
    if (totalRestaurants === 0) return null;
    const headerClass = ['text-dark py-2 pr-4 m-0', currentPage ? 'border-gray border-right' : ''].join(' ').trim();
  return (
    <div className="table-responsive">
      <table className="table table-striped table-sm">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Address</th>
            <th>Description</th>
            <th>Guest Capacity</th>
            <th>Booking Capacity</th>
            <th>Reservations</th>
          </tr>
        </thead>
        <tbody>
          {currentRestaurants.map((restaurant, i) => {
            return(
              <tr key={i}>
                <td>{restaurant.id}</td>
                <td>{restaurant.name}</td>
                <td>{restaurant.address}</td>
                <td>{restaurant.description}</td>
                <td>{restaurant.guest_capacity}</td>
                <td>{restaurant.booking_capacity}</td>
                <td>
                  <div className="check-action" title="Check resevation">
                    <Link to={{
                      pathname: '/restaurants/'+restaurant.id+'/reservations',
                      restaurantID: {id: restaurant.id}
                    }}>
                      <img src={checkAction} alt="Check Reservations" className="image-center" />
                    </Link>
                  </div>
                  <div className="create-action" title="Create resevation">
                  <Link to={{
                    pathname: '/restaurants/'+restaurant.id+'/reservations/create',
                    restaurantID: {id: restaurant.id}
                  }}>
                    <img src={createAction} alt="Create Reservations" className="image-center" />
                  </Link>
                  </div>
                </td>
              </tr>
            )
           })}
        </tbody>
      </table>

      <div className="container mb-5 pagination-custom">
        <div className="row d-flex flex-row py-5">
          <div className="w-100 px-4 py-5 d-flex flex-row flex-wrap align-items-center justify-content-between">
            <div className="d-flex flex-row align-items-center">
              <h2 className={headerClass}>
                <strong className="text-secondary">{totalRestaurants}</strong> Restaurant
              </h2>
              { currentPage && (
                <span className="current-page d-inline-block h-100 pl-4 text-secondary">
                  Page <span className="font-weight-bold">{ currentPage }</span> / <span className="font-weight-bold">{ totalPages }</span>
                </span>
              ) }
            </div>
            <div className="d-flex flex-row py-4 align-items-center">
              <Pagination totalRecords={totalRestaurants} pageLimit={10} pageNeighbours={1} onPageChanged={this.onPageChanged} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
 }
}

export default RestaurantsContainer
