import React, { Component } from 'react'
import axios from 'axios'
import Navigation from '../pagelayout/Navigation'
import MainMenu from '../pagelayout/MainMenu'
import AuthService from '../AuthService.js';
import Swal from 'sweetalert2'
import Pagination from '../Pagination'
import deleteAction from '../../custom_img/delete.png'
import editAction from '../../custom_img/edit.png'
import '../../custom_css/reservation.scss';
import '../../custom_css/pagination.scss';
// eslint-disable-next-line
import { BrowserRouter as Route, Link } from "react-router-dom";
const Auth = new AuthService();

class Reservations extends Component {
  constructor(props) {
    super(props)
    this.state = {
      allReservations: [],
      currentReservations: [],
      restaurantId: props.location.restaurantID,
      currentPage: null,
      totalPages: null,
      token: {
          'Content-Type': 'application/json',
          'Authorization': localStorage.getItem('id_token')
      }
    }
  }

  componentDidMount() {
    if (!Auth.loggedIn()) {
        this.props.history.replace('/login')
    } else {
      axios.get('http://localhost:3001'+this.props.location.pathname, { headers: this.state.token })
      .then(response => {
        const { data: allReservations = [] } = response.data
        this.setState({ allReservations });
      })
      .catch(err => {
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: this.handleError(err),
          footer: '<a href="/">Go back to restaurants list</a>'
        })
      })
    }
  }

  onPageChanged = data => {
    const { currentPage, pageLimit } = data;
    if (!Auth.loggedIn()) {
        this.props.history.replace('/login')
    } else {
      axios.get(`http://localhost:3001`+this.props.location.pathname+`?page=${currentPage}&per_page=${pageLimit}`, { headers: this.state.token })
      .then(response => {
        const { allReservations } = this.state;
        const { currentPage, totalPages, pageLimit } = data;
        const offset = (currentPage - 1) * pageLimit;
        const currentReservations = allReservations.slice(offset, offset + pageLimit);
        this.setState({ currentPage, currentReservations, totalPages });
      })
      .catch(err => {
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: this.handleError(err),
          footer: '<a href="/">Go back to restaurants list</a>'
        })
      })
    }
  }

  handleLogout() {
    Auth.logout()
    this.props.history.replace('/login');
  }

  handleError(err) {
    if (!err.hasOwnProperty('response')) {
      err.response = {
          data: {
            details: "An Unexpected Error Occurred"
          }
       }
    }
    if (Object.keys(err.response.data.details).length !== 0) {
      var response_error = JSON.stringify(err.response.data.details);
      if (response_error) {
        response_error = response_error.slice(1, response_error.length-1).replace(/"/g, "")
        response_error = response_error.split(',')
        response_error = response_error.join("\r\n");
        return response_error
      }
    } else {
      return err.response.data.message
    }
  }

  handleDeleteResevation(reservation_id) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this action!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete reservation!'
      }).then((result) => {
        if (result.value) {
          axios.delete('http://localhost:3001/reservations/'+reservation_id, { headers: this.state.token })
          .then(res => {
            window.location.reload();
            Swal.fire(
              'Deleted!',
              'Your reservation has been deleted',
              'success'
            )
          })
          .catch(err => {
            Swal.fire({
              type: 'error',
              title: 'Oops...',
              text: this.handleError(err),
              footer: '<a href>Try to refresh the page</a>'
            })
          })
        }
      })
  }

  render() {
    const { allReservations, currentReservations, currentPage, totalPages } = this.state;
    const totalReservations = allReservations.length;
    if (totalReservations === 0) return null;
    const headerClass = ['text-dark py-2 pr-4 m-0', currentPage ? 'border-gray border-right' : ''].join(' ').trim();
  return (
    <div className="App">
        <div className="container-fluid">
          <div className="row">
          <Navigation />
          <MainMenu />
            <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
              <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 className="h2">Reservations</h1>
                <div className="btn-toolbar mb-2 mb-md-0">
                  <div className="btn-group mr-2">
                    <button type="button" className="btn btn-sm btn-outline-secondary">Share</button>
                    <button type="button" className="btn btn-sm btn-outline-secondary">Export</button>
                  </div>
                  <button type="button" className="btn btn-sm btn-outline-secondary dropdown-toggle">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-calendar">
                      <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                      <line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line>
                      <line x1="3" y1="10" x2="21" y2="10"></line>
                    </svg>
                    This week
                  </button>
                  <button type="button" className="btn btn-sm btn-logout btn-outline-secondary" onClick={this.handleLogout.bind(this)}>Logout</button>
                </div>
              </div>
              <div className="table-responsive">
                <table className="table table-striped table-sm">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Surname</th>
                      <th>Email</th>
                      <th>Guest Number</th>
                      <th>Restaurant ID</th>
                      <th>Created</th>
                      <th>Navigation</th>
                    </tr>
                  </thead>
                  <tbody>
                    {currentReservations.map((reservation, i) => {
                      return(
                        <tr key={i}>
                          <td>{reservation.id}</td>
                          <td>{reservation.name}</td>
                          <td>{reservation.surname}</td>
                          <td>{reservation.email}</td>
                          <td>{reservation.guest_number}</td>
                          <td>{reservation.restaurant_id}</td>
                          <td>{reservation.created_at}</td>
                          <td>
                            <div className="delete-action" title="Delete resevation">
                              <Link to={{pathname: ''}} onClick={this.handleDeleteResevation.bind(this, reservation.id)}>
                                <img src={deleteAction} alt="Logo" className="image-center" />
                              </Link>
                            </div>
                            <div className="edit-action" title="Edit resevation">
                              <Link to={{pathname: '/reservations/'+reservation.id+''}}>
                                <img src={editAction} alt="Logo" className="image-center " />
                              </Link>
                            </div>
                          </td>
                        </tr>
                      )
                     })}
                  </tbody>
                </table>
                <div className="container mb-5 pagination-custom">
                  <div className="row d-flex flex-row py-5">
                    <div className="w-100 px-4 py-5 d-flex flex-row flex-wrap align-items-center justify-content-between">
                      <div className="d-flex flex-row align-items-center">
                        <h2 className={headerClass}>
                          <strong className="text-secondary">{totalReservations}</strong> Reservation
                        </h2>
                        { currentPage && (
                          <span className="current-page d-inline-block h-100 pl-4 text-secondary">
                            Page <span className="font-weight-bold">{ currentPage }</span> / <span className="font-weight-bold">{ totalPages }</span>
                          </span>
                        ) }
                      </div>
                      <div className="d-flex flex-row py-4 align-items-center">
                        <Pagination totalRecords={totalReservations} pageLimit={5} pageNeighbours={1} onPageChanged={this.onPageChanged} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </main>
          </div>
        </div>
    </div>
  );
 }
}

export default Reservations
