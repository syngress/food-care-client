import React, { Component } from 'react'
import axios from 'axios'
import Navigation from '../pagelayout/Navigation'
import MainMenu from '../pagelayout/MainMenu'
import AuthService from '../AuthService.js';
import FormErrors from '../FormErrors'
import Swal from 'sweetalert2'
// eslint-disable-next-line
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
const Auth = new AuthService();

class ReservationsEdit extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      surname: '',
      email: '',
      guestNumber: '',
      formErrors: {name: '', surname: '', email: '', guestNumber: ''},
      nameValid: true,
      surnameValid: true,
      emailValid: true,
      guestNumberValid: true,
      formValid: true,
      token: {
          'Content-Type': 'application/json',
          'Authorization': localStorage.getItem('id_token')
      }
    }
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  componentDidMount() {
    if (!Auth.loggedIn()) {
        this.props.history.replace('/login')
    } else {
      axios.get('http://localhost:3001'+this.props.location.pathname, { headers: this.state.token })
      .then(res => {
        this.setState({
          name: res.data.name,
          surname: res.data.surname,
          email: res.data.email,
          guestNumber: res.data.guest_number
        })
      })
      .catch(err => {
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: this.handleError(err),
          footer: '<a href>Try to refresh the page</a>'
        })
      })
    }
  }

  handleUserInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]: value},
      () => { this.validateField(name, value) });
  }

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let nameValid = this.state.nameValid;
    let surnameValid = this.state.surnameValid;
    let emailValid = this.state.emailValid;
    let guestNumberValid = this.state.guestNumberValid;

    switch(fieldName) {
      case 'name':
        nameValid = value.length >= 2
        fieldValidationErrors.name = nameValid ? '' : ' is invalid';
        break;
      case 'surname':
        surnameValid = value.length >= 2;
        fieldValidationErrors.surname = surnameValid ? '' : 'is invalid';
        break;
      case 'email':
        emailValid = value.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i);
        fieldValidationErrors.email = emailValid ? '' : ' is invalid';
        break;
      case 'guestNumber':
        value <= 0 ? guestNumberValid = false : guestNumberValid = true
        fieldValidationErrors.guestNumber = guestNumberValid ? '': ' provide guest number';
        if (value >= 9) {
          fieldValidationErrors.guestNumber = 'exceeded';
        }
        break;
      default:
        break;
    }

    this.setState({
      formErrors: fieldValidationErrors,
      nameValid: nameValid,
      surnameValid: surnameValid,
      emailValid: emailValid,
      guestNumberValid: guestNumberValid,
    }, this.validateForm);
  }

  validateForm() {
    this.setState({formValid: this.state.nameValid && this.state.surnameValid && this.state.emailValid && this.state.guestNumberValid});
  }

  handleLogout() {
    Auth.logout()
    this.props.history.replace('/login');
  }

  handleError(err) {
    if (!err.hasOwnProperty('response')) {
      err.response = {
          data: {
            details: "An Unexpected Error Occurred"
          }
       }
    }
    if (Object.keys(err.response.data.details).length !== 0) {
      var response_error = JSON.stringify(err.response.data.details);
      if (response_error) {
        response_error = response_error.slice(1, response_error.length-1).replace(/"/g, "")
        response_error = response_error.split(',')
        response_error = response_error.join("\r\n");
        return response_error
      }
    } else {
      return err.response.data.message
    }
  }

  handleFormSubmit(e) {
      e.preventDefault();

      var data = {
        name: this.state.name,
        surname: this.state.surname,
        email: this.state.email,
        guest_number: this.state.guestNumber
      }

      axios.put('http://localhost:3001'+this.props.location.pathname, data, {
        headers: this.state.token
      }).then(res => {
        let timerInterval
        Swal.fire({
          title: 'Update completed.',
          html: 'Page refresh automatically in <strong></strong> seconds.',
          type: "success",
          timer: 1500,
          onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
              Swal.getContent().querySelector('strong')
                .textContent = Swal.getTimerLeft()
            }, 100)
          },
          onClose: () => {
            clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
            window.history.back();
          }
        })
      })
      .catch(err => {
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: this.handleError(err),
          footer: '<a href>Try to refresh the page</a>'
        })
      })
  }

  errorClass(error) {
    return(error.length === 0 ? '' : 'is-invalid');
  }

  handleGoBack() {
    window.history.back();
  }

  render() {
    var divStyle = {
      width: '100%'
    };
    return (
      <div className="App">
          <div className="container-fluid">
            <div className="row">
            <Navigation />
            <MainMenu />
              <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                  <h1 className="h2">Edit Reservations</h1>
                  <div className="btn-toolbar mb-2 mb-md-0">
                    <div className="btn-group mr-2">
                      <button type="button" className="btn btn-sm btn-outline-secondary">Share</button>
                      <button type="button" className="btn btn-sm btn-outline-secondary">Export</button>
                    </div>
                    <button type="button" className="btn btn-sm btn-outline-secondary dropdown-toggle">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-calendar">
                        <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                        <line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line>
                        <line x1="3" y1="10" x2="21" y2="10"></line>
                      </svg>
                      This week
                    </button>
                    <button type="button" className="btn btn-sm btn-logout btn-outline-secondary" onClick={this.handleLogout.bind(this)}>Logout</button>
                  </div>
                </div>
                <div className="table-responsive">
                <div className="registration-form">
                  <div className="col-md-8 order-md-1">
                    <h6 className="mb-3"><b>Customer:</b> {this.state.name} {this.state.surname}</h6>
                    <hr className="mb-4"></hr>
                    <div className="panel panel-default">
                      <FormErrors formErrors={this.state.formErrors} />
                    </div>
                    <form className="needs-validation">
                      <div className="row">
                        <div className="col-md-6 mb-3">
                          <label>Name</label>
                          <input
                            type="text"
                            className={`form-control ${this.errorClass(this.state.formErrors.name)}`}
                            id="name"
                            name="name"
                            placeholder="Your Name"
                            onChange={this.handleUserInput}
                            value={this.state.name}
                            required={true} />
                            <div className="invalid-feedback" style={divStyle}>
                              Your name is required.
                            </div>
                        </div>
                        <div className="col-md-6 mb-3">
                          <label>Surname</label>
                          <div className="input-group">
                            <input
                              type="text"
                              className={`form-control ${this.errorClass(this.state.formErrors.surname)}`}
                              id="surname"
                              name="surname"
                              placeholder="Surname"
                              onChange={this.handleUserInput}
                              value={this.state.surname}
                              required={true} / >
                            <div className="invalid-feedback" style={divStyle}>
                              Your surname is required.
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="mb-3">
                        <label>Email</label>
                        <input
                          type="email"
                          className={`form-control ${this.errorClass(this.state.formErrors.email)}`}
                          id="email"
                          name="email"
                          placeholder="your@email.com"
                          value={this.state.email}
                          onChange={this.handleUserInput}
                          required={true} />
                        <div className="invalid-feedback">
                          Please enter a valid email address.
                        </div>
                      </div>
                      <div className="mb-3">
                      <div className="row">
                        <div className="col-md-6 mb-3">
                          <label>Guest number</label>
                          <input
                            type="number"
                            className={`form-control ${this.errorClass(this.state.formErrors.guestNumber)}`}
                            id="guestNumber"
                            name="guestNumber"
                            placeholder="0"
                            onChange={this.handleUserInput}
                            value={this.state.guestNumber}
                            required={true} />
                            <div className="invalid-feedback" style={divStyle}>
                              Invalid guest number.
                            </div>
                        </div>
                      </div>
                      </div>
                      <hr className="mb-4"></hr>
                      <div className="row">
                        <hr className="mb-4"></hr>
                        <div className="col-md-6 mb-3">
                          <button className="btn btn-primary btn-lg btn-block" type="submit" disabled={!this.state.formValid} onClick={this.handleFormSubmit}>Save</button>
                        </div>
                        <div className="col-md-6 mb-3">
                        <Link to={{pathname: ''}} className="btn btn-lg btn-block" onClick={this.handleGoBack.bind(this)}>Get Back</Link>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                </div>
              </main>
            </div>
          </div>
      </div>
    );
   }
 }

export default ReservationsEdit
