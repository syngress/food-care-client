import * as React from "react";
import AuthService from './components/AuthService';
import RestaurantsContainer from './components/restaurant/RestaurantsContainer'
import Navigation from './components/pagelayout/Navigation'
import MainMenu from './components/pagelayout/MainMenu'
import './custom_css/logout.scss';
import { connect } from "react-redux";
import { restaurantsFetched } from "./actions";
import axios from 'axios'
const Auth = new AuthService();

export class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      token: {
          'Content-Type': 'application/json',
          'Authorization': localStorage.getItem('id_token')
      }
    }
  }

  handleLogout(){
    Auth.logout()
    this.props.history.replace('/login');
  }

  componentDidMount() {
    if (this.state.token) {
      axios.get(`http://localhost:3001/restaurants`, { headers: this.state.token })
      .then(response => {
        this.props.restaurantsFetched(response.data)
      })
      .catch(error => console.log(error))
    }
  }

  render() {
    return (
      <div className="App">
          <div className="container-fluid">
            <div className="row">
            <Navigation />
            <MainMenu />
              <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                  <h1 className="h2">Restaurants</h1>
                  <div className="btn-toolbar mb-2 mb-md-0">
                    <div className="btn-group mr-2">
                      <button type="button" className="btn btn-sm btn-outline-secondary">Share</button>
                      <button type="button" className="btn btn-sm btn-outline-secondary">Export</button>
                    </div>
                    <button type="button" className="btn btn-sm btn-outline-secondary dropdown-toggle">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                      This week
                    </button>
                    <button type="button" className="btn btn-sm btn-logout btn-outline-secondary" onClick={this.handleLogout.bind(this)}>Logout</button>
                  </div>
                </div>
                  <RestaurantsContainer restaurants={this.props.restaurants}/>
              </main>
            </div>
          </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    restaurants: state.restaurants
  }
};

const mapDispatchToProps = { restaurantsFetched };

export const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);
