import React from "react";
import ReactDOM from 'react-dom';
import { AppContainer } from "./App";
import * as serviceWorker from './serviceWorker';
import UserLogin from './components/user/UserLogin';
import Reservations from './components/reservation/Reservations';
import ReservationEdit from './components/reservation/ReservationEdit';
import ReservationCreate from './components/reservation/ReservationCreate';
import UserRegistration from './components/user/UserRegistration';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { store } from "./store";
import { Provider } from "react-redux";

ReactDOM.render(
  <Provider store={store}>
    <Router>
        <div>
            <Route exact path="/" component={AppContainer} />
            <Route exact path="/login" component={UserLogin} />
            <Route exact path="/register" component={UserRegistration} />
            <Route exact path="/restaurants/:id/reservations" component={Reservations} />
            <Route exact path="/restaurants/:id/reservations/create" component={ReservationCreate} />
            <Route exact path="/reservations/:id" component={ReservationEdit} />
        </div>
    </Router>
    </Provider>,
      document.getElementById('root')
);

serviceWorker.unregister();
