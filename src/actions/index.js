export const restaurantsFetched = (restaurants) => ({
  type: 'FETCH_RESTAURANTS_SUCCESS',
  restaurants
});
