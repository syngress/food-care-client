# Food Care Client v1.0
![ReactIMG](https://szymonkrajewski.pl/wp-content/uploads/2017/10/012_logo_reactstrap-1.png)

Things you may want to cover:

# SOFTWARE

* NodeJS
* Npm or Yarn node package manager
* Food Care API application

--------

# Project assumptions

Food Care Client Application provides UI for Food Care API.

Application allows you to manage restaurants and their reservations.

# Configuration

Backend application should serve data on port 3001

## Example of use

To use application you have to log in to your account.

![FoodCare1](https://syngress.pl/images/foodcare_1.png)

Registration form will allow you to create a new user.

![FoodCare9](https://syngress.pl/images/foodcare_9.png)

Every field of from is validated separately.

![FoodCare10](https://syngress.pl/images/foodcare_10.png)

Validation of registration form will ensure that the entered passwords agree with each other

![FoodCare11](https://syngress.pl/images/foodcare_11.png)

A correctly completed form will be processed by the API. Success user registration will be preceded by the following message.

![FoodCare12](https://syngress.pl/images/foodcare_12.png)

Redirection from the registration page to the login page will take place automatically.

Correct authentication allows access to the list of your restaurants.

![FoodCare2](https://syngress.pl/images/foodcare_2.png)

Each of your restaurant allows you to view active reservations.

![FoodCare4](https://syngress.pl/images/foodcare_4.png)

Returned result is paginated

![FoodCare4](https://syngress.pl/images/foodcare_16.png)

Backend returns reservations only assigned to the selected restaurant for the logged-in user. Returned result is paginated.

|Method          |Endpoint                                  |Note                                             |
|----------------|------------------------------------------|-------------------------------------------------|
|GET			       |`restaurants/:id/reservations`            |Returns reservations for the selected restaurant |

![FoodCare5](https://syngress.pl/images/foodcare_5.png)

Pressing the delete button, application display modal window to confirm

![FoodCare6](https://syngress.pl/images/foodcare_6.png)

If you confirm, application will automatically delete and refresh your reservations by displaying an appropriate message

![FoodCare7](https://syngress.pl/images/foodcare_7.png)

An attempt to delete reservation that does not belong to your restaurant or a reservation that does not exist in the system will result in an error.

![FoodCare8](https://syngress.pl/images/foodcare_8.png)

There is a possibility to edit the reservation.

![FoodCare13](https://syngress.pl/images/foodcare_13.png)

Backend allows you to edit only the reservations that belong to your restaurant.

|Method          |Endpoint                                  |Note                                             |
|----------------|------------------------------------------|-------------------------------------------------|
|PUT			       |`reservations/:id`                        |Edit reservation for the selected restaurant     |

![FoodCare14](https://syngress.pl/images/foodcare_14.png)

Every field of from is validated separately. Saving changes button becomes inactive if the form does not pass the validation

![FoodCare15](https://syngress.pl/images/foodcare_15.png)

Application allows you to create a new reservation for authorize restaurant.

![FoodCareCreateReservationButton](https://syngress.pl/images/foodcare_create_reservation_button.png)

|Method          |Endpoint                                  |Note                                             |
|----------------|------------------------------------------|-------------------------------------------------|
|POST			       |`/restaurants/:id/reservations/create`    |Create reservation for the selected restaurant   |

![FoodCareCreateReservationForm](https://syngress.pl/images/foodcare_create_reservation_form.png)

Every field of from is validated separately. Saving changes button becomes inactive if the form does not pass the validation.

![FoodCareCreateReservationValidation](https://syngress.pl/images/foodcare_create_reservation_validation.png)

You can always close your current session by clicking "Logout" button

![FoodCare3](https://syngress.pl/images/foodcare_3.png)

**THIS PROJECT IS STILL UNDER CONSTRUCTION**

Backend for this GUI can be found here:
https://bitbucket.org/syngress/food-care-api/src/master/
